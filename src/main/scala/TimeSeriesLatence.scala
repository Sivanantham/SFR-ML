import java.util.Date

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
//import org.joda.time.DateTime
//import org.joda.time.format.DateTimeFormat

/**
  * Created by sylvain on 19/07/17.
  */
object TimeSeriesLatence {
  def Series(parse:RDD[(String, Long, String, String, String,Int ,String)],streamer:String) = {

    val tableFiltered=parse.filter(u=>(!(u._1.equals("SETUP") )))
   val durSess=dureeSession(tableFiltered).collect()
    val corres=recupSessRFGW(parse).collect()
    val resSess=tableFiltered.map(u=>{

      var position:Float= -1
      if(u._7.contains("npt=")&&(u._1.equals("GET_ANSWER"))){
        position=u._7.split("npt=")(1).split("-")(0).toFloat
      }
      //((session,cseq),(type de requete,timestamp,timestamp,message,code http,position dans la video))
      ((u._5,u._4),(u._1,u._2,u._2,u._7,u._6,position))
    }).reduceByKey((a,b)=>{
      var tmin=a._2
      var tmax=a._3
      if(b._3>a._3){
        tmax=b._3
      }
      if(b._2<a._2){
        tmin=b._2
      }
      var codeerr=a._5
      if(a._5==(-1)){
        codeerr=b._5
      }
      var pos=a._6
      if(a._6== -1){
        pos=b._6
      }
      //(concatenation du type de requete,timestamp de envoie de réquete, timestamp de réponse de requete,
      // concatenation des messages,code http de la réponse,position de la réponse)
      (a._1+";"+b._1,tmin,tmax,a._4+";"+b._4,codeerr,pos)
    }).map(u=>{
      //(session,cseq,type de requete, ts d'envoie de requete, ts réponse de requete,latende de la requete, nom du streamer,
      // code http ,message, position )
      (u._1._1,u._1._2.toLong,u._2._1,u._2._2.toLong,u._2._3-u._2._2,streamer,u._2._5,u._2._4,u._2._6)
    })
    val desc=seriesAmeliore(resSess).collect()
    val fin=resSess.map(u=>{
      var rfgwsg=""
      corres.foreach{v=>
        if(v._1.equals(u._1)){
          rfgwsg=v._2
        }
      }
      var rfgw=""
      var sg=""
      if (!rfgwsg.equals("")) {
        rfgw =rfgwsg.substring(0,rfgwsg.length-5)
        sg=rfgwsg.substring(rfgwsg.length-5,rfgwsg.length-2)
      }
      var duree:Float= -1
      desc.foreach(v=>{
        if (v._1.equals(u._1)){
          duree=v._2
        }
      })
      var visionnage: Float= -1
      var tmin:Long=0
      var tmax:Long=0
      var dureesess:Long=0
      durSess.foreach(v=>{
        if(v._1.equals(u._1)){
          tmin=v._2
          tmax=v._3
          dureesess=v._4
        }
      })
      //(session,cseq,type de requete, ts d'envoie de requete, ts réponse de requete,latende de la requete, nom du streamer,
      // code http, num rfgw, num sg, durée de la video, position dans la video,ratio visionage, ts de début de session,
      // ts de fin de session, durée de la session, message)
      (u._1,u._2,u._3,u._4,u._5,u._6,u._7,rfgw,sg,duree,u._9,u._9/duree,tmin,tmax,dureesess,u._8)
    })
    fin
  }
  def seriesAmeliore(parse: RDD[(String, Long, String, Long, Long, String, Int, String,Float)])={
    val desc=parse.filter(u=>u._3.contains("DESCRIBE")).map(u=>{
      var duree:Float= -1
      u._8.split(";").foreach(v=>{
        if(v.contains("range:npt=")){
          duree=v.split("npt=")(1).split("-")(1).toFloat
        }
      })
      // (num de session, durée de la video)
      (u._1,duree)
    })
    desc
  }
  def recupSessRFGW(parse:RDD[(String, Long, String, String, String,Int, String)])={

    val tableFiltered=parse.filter(u=>u._1.equals("SETUP_ANS"))
    val resSess=tableFiltered.map(u=>((u._5),(u._7,1))).reduceByKey((a,b)=>{
      (a._1+";"+b._1,a._2+b._2)
    }).map(u=>{
      var rfgw=u._2._1
      var a:Array[String]=Array()
      if(u._2._2==1) {
        rfgw = u._2._1.split(";")(2)
      }
      (u._1,rfgw)
    })
    //(num session, rfgw+sg)
    resSess
  }
  def dureeSession(parse:RDD[(String, Long, String, String, String,Int, String)])={

    val resSess=parse.map(u=>((u._5),(u._2,u._2))).reduceByKey((a,b)=>{
      var tmin=a._1
      var tmax=a._2
      if(b._2>a._2){
        tmax=b._2
      }
      if(b._1<a._1){
        tmin=b._1

      }

      (tmin,tmax)
    }).map(u=>{

      (u._1,u._2._1.toLong,u._2._2.toLong,(u._2._2-u._2._1).toLong)
    })
    //(num session, début session, fin session, durée session)
    resSess
  }

}
