import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import java.time._
import java.sql._
/**
  * Created by sylvain on 13/07/17.
  */
object Parsing {
  def parse(input:String): RDD[(String, Long, String, String, String, Int,String)] = {
    val conf = new SparkConf().setAppName("Indexation")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    val sc = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()
    sc.sparkContext.setLogLevel("ERROR")
    sc.sparkContext.hadoopConfiguration.set("textinputformat.record.delimiter", "2017-")
    val cinqcent = sc.sparkContext.textFile(input).map(u => u.replace("\r\n", ";")).map(u => u.replace("\n", ";")).map(line => {
      var logAgarde = 0
      if (line.contains("|Message:;") || line.contains("|Messages:;") || line.contains("|SETUP Response:;") || line.contains("|DESCRIBE Response:;")) {
        logAgarde = 1
      }
      (line, logAgarde)
    })
      .filter(_._2 == 1)
    val regexGET =
      """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*CSeq: ([0-9]+).*Session: ([0-9]+)(.*)stream_state;position.*""".r

    val regexSETUPANS = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*SETUP Response:.*CSeq: ([0-9]+).*Session: ([0-9]+);timeout=([0-9]+).*client=([0-9]+).([0-9]+);destination=([0-9]+.[0-9]+.[0-9]+.[0-9]+).*rtsp:\/\/([0-9]+.[0-9]+.[0-9]+.[0-9]+).*""".r
    val regexSETUP ="""(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*SETUP(.*).*""".r
    val regexGETANS = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*RTSP/1.0 ([0-9]+).*CSeq: ([0-9]+).*Session: ([0-9]+)(.*).*""".r

    val regexPLAY = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*PLAY .*CSeq: ([0-9]+).*Session: ([0-9]+).*Range:(.*)-.*Scale:(.*).*""".r


    val regexDESC = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*DESCRIBE.*CSeq: ([0-9]+).*Session: ([0-9]+).*""".r

    val regexDESCANS = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*CSeq: ([0-9]+).*Session: ([0-9]+).*i=(\w+.\w+).*a=range:npt=(\d+.\d+)-(\d+.\d+).*""".r

    val regexANNOUNCE = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*ANNOUNCE .*CSeq: ([0-9]+).*Session: ([0-9]+).*Notice:(.*).*""".r

    val regexTEARDOWN = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*TEARDOWN .*CSeq: ([0-9]+).*Session: ([0-9]+).*""".r

    val regexPAUSE = """(?s).*([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+).*\>(\w+).cc.*:.*PAUSE .*CSeq: ([0-9]+).*Session: ([0-9]+).*;(.*)""".r

    def function(log: String): List[String] = {
      log match {
        case regexSETUP(mois, jour, heure, minute, seconde, ms, proto, message) => List(mois, jour, heure, minute, seconde, ms, proto, message)
        case regexSETUPANS(mois, jour, heure, minute, seconde, ms, proto, setup, transport, destination, client_port, cseq, user_agent) => List(mois, jour, heure, minute, seconde, ms, proto, setup, transport, destination, client_port, cseq, user_agent)
        case regexPLAY(mois, jour, heure, minute, seconde, ms, proto, cseq, session, range, scale) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session, range, scale)
        case regexDESC(mois, jour, heure, minute, seconde, ms, proto, cseq, session) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session)
        case regexANNOUNCE(mois, jour, heure, minute, seconde, ms, proto, cseq, session, message) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session, message)
        case regexGETANS(mois, jour, heure, minute, seconde, ms, proto, response, cseq, session,message) => List(mois, jour, heure, minute, seconde, ms, proto, response, cseq, session,message)
        case regexTEARDOWN(mois, jour, heure, minute, seconde, ms, proto, cseq, session) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session)
        case regexPAUSE(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message)
        case regexGET(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message) => List(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message)
        case _ => null
      }
    }
    val processed: RDD[List[String]] = cinqcent.map(_._1).collect {
      case regexSETUPANS(mois, jour, heure, minute, seconde, ms, proto, cseq, session, to, idclient, rfgwsg, ip, ipstr) => List("SETUP_ANS", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session, to, idclient, rfgwsg, ip, ipstr)
      case regexPLAY(mois, jour, heure, minute, seconde, ms, proto, cseq, session, range, scale) => List("PLAY", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session, range, scale)
      case regexDESC(mois, jour, heure, minute, seconde, ms, proto, cseq, session) => List("DESCRIBE", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session)
      case regexANNOUNCE(mois, jour, heure, minute, seconde, ms, proto, cseq, session, message) => List("ANNOUNCE", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session,message)
      case regexTEARDOWN(mois, jour, heure, minute, seconde, ms, proto, cseq, session) => List("TEARDOWN", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session)
      case regexSETUP(mois, jour, heure, minute, seconde, ms, proto, message) => List("SETUP", "2017", mois, jour, heure, minute, seconde, ms, proto, message)
      case regexPAUSE(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message) => List("PAUSE", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session,message)
      case regexGETANS(mois, jour, heure, minute, seconde, ms, proto, response, cseq, session,message) => List("GET_ANSWER", "2017", mois, jour, heure, minute, seconde, ms, proto, response, cseq, session,message)
      case regexGET(mois, jour, heure, minute, seconde, ms, proto, cseq, session,message) => List("GET", "2017", mois, jour, heure, minute, seconde, ms, proto, cseq, session,message)

    }


    val tableStructure = processed.map(u => {
      val ts: Long =Timestamp.valueOf(LocalDateTime.of(2017,u(2).toInt,u(3).toInt,u(4).toInt,u(5).toInt,u(6).toInt,u(7).toInt)).getTime()

      if (u(0).equals("GET_ANSWER")) {
        var v = ""

        for (i <- u.slice(12, u.size)) {
          v += i+";"
        }
        (u(0), ts, u(8), u(10), u(11),u(9).toInt, v)
      } else if (u(0).equals("SETUP") && (u.size > 9)) {

        (u(0), ts, u(8), "", "",-1, u(9))
      } else if (u(0).equals("START")) {
        var v = u(11)

        for (i <- u.slice(12, u.size)) {
          v += ";" + i
        }
        (u(0), ts, u(8), u(10), u(11),-1, v)
      } else if (u.size > 11) {
        var v = u(11)
        for (i <- u.slice(12, u.size)) {
          v += ";" + i
        }
        (u(0), ts, u(8), u(9), u(10),-1, v)
      } else {
        (u(0), ts, u(8), u(9), u(10),-1 ,"")
      }
    })

    // Cette table renvoie [Type de la Requete,Timestamp, Nom de la machine, Cseq, Session, Code HTTP, Message]
    tableStructure
  }

}

