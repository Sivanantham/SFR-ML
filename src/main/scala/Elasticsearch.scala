import java.util

import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.elasticsearch.spark._
import org.elasticsearch.spark.sql._

import scala.collection.immutable.HashMap
/**
  * Created by sylvain on 20/07/17.
  */
object Elasticsearch {
  def mapperArgs(args : Array[String]): HashMap[String, String] ={
    var argsMap = new HashMap[String, String]();
    for (arg <- args){
      val argMembers = arg.split("=")
      argsMap.+=((argMembers(0),argMembers(1)))
    }
    return argsMap
  }
  def getArg(argsMap :HashMap[String, String],nom:String):String= {
    var arg: String = null
    if (argsMap.get(nom) != None) {
      arg = argsMap.get(nom).get
      System.out.println(nom + " = " + arg)
    } else {
      System.out.println(nom +" is not specified")
      System.exit(0);
    }
    return arg
  }
  def main(args: Array[String]): Unit = {
    var argsMap = mapperArgs(args)
    val input=getArg(argsMap,"Input")
    val streamer=getArg(argsMap,"Streamer")
    /*val port="9200"
    val nodes= "10.33.1.145"*/
    val conf = new SparkConf().setAppName("Indexation").setMaster("local[*]")
    /*conf.set("es.nodes", nodes)
    conf.set("es.port", port)
    conf.set("es.net.http.auth.user", "sylvain")
    conf.set("es.net.http.auth.pass", "changeme").set("es.spark.dataframe.write.null", "true")*/

    val sc = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()
    val parse=Parsing.parse(input)
    val TSlatence=sc.createDataFrame(TimeSeriesLatence.Series(parse,streamer)).toDF("session","cseq","request","ts","latence","streamer","code_HTTP","rfgw","sg","dureeContenu","position","ratio_visionnage","debutSess","finSess","dureeSess","message")
    TSlatence.show(1000)
    //TSlatence.saveToEs("sfr/logs")


  }
}
