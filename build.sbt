
name := "SFRSparkv3"

version := "1.0"

scalaVersion := "2.11.8"

val sparkV = "2.1.0"
libraryDependencies ++= Seq(  "org.apache.spark" %% "spark-core" % sparkV,
  "org.apache.spark" %% "spark-sql" % sparkV,
  "org.apache.spark" %% "spark-mllib" % sparkV,
  "org.apache.spark" %% "spark-hive" % sparkV,
  "com.typesafe" % "config" % "1.3.1",
  "org.elasticsearch" % "elasticsearch-spark-20_2.11" % "6.0.0-alpha2"
)
        